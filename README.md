# MAS507 - Product Development and Project Management
This repo contains code and various guides for the studets to use in the course [MAS507](https://www.uia.no/en/studieplaner/topic/MAS507-G) teached at the [Univeristy of Agder](https://www.uia.no/en), Campus Grimstad. The course homepage with documentation, guides and code explanations are found at https://uia-mekatronikk.gitlab.io/mas507/index.html

## Lecturers over the years
| Year | Lecturer 1                  | Lecturer 2               |
| ---- | :-------------------------- | :----------------------- |
| 2020 | [Sondre Sanden Tørdal][sst] | [Mette Mo Jakobsen][mmj] |
| 2019 | [Andreas Klausen][ak]       | [Mette Mo Jakobsen][mmj] |


<!-- Hyperlinks  -->
[sst]: https://www.uia.no/kk/profil/sondrest
[mmj]: https://www.uia.no/kk/profil/mettemj
[ak]: https://www.uia.no/kk/profil/andrek10