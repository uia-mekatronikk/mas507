.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: ./figs/jetbot.jpg
   :width: 12cm
   :align: center

|
|

MAS507 - Product Development and Project Management
===================================================

Student Group Project Reports 2019
----------------------------------

- https://evenfl.gitlab.io/mas507/
- https://krikru.gitlab.io/mas507/
- https://elena1992elena.gitlab.io/mas507/
- https://stian_kristensen.gitlab.io/mas507/
- https://gurgle96.gitlab.io/mas507/


Lecturers
---------

- Sondre Sanden Tørdal, PhD
   - Postdoctoral Fellow in Mehcatronics
   - https://sondrest.gitlab.io/portfolio/

- Andreas Klausen, PhD
   - Postdoctoral Fellow in Mehcatronics
   - https://andreasklausen.no/

- Mette Mo Jakobsen, PhD
   - Professor in Product Development and Project-based learning
   - https://www.uia.no/kk/profil/mettemj



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/start
   src/report
   src/ros
   src/calibration
   src/web-controller
   src/git
   src/strawberry
   src/demos
   src/troubleshooting
   src/bibliography

.. Indices and tables
.. ==================



.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
